// Need to be implement
window.status = 'all';
window.addEventListener('DOMContentLoaded', showItems(window.status));

function enterItem(event) {
  if(event.keyCode === 13) {
    addToDoItem();
  }
};
function itemGenerator(item) {
  return `<li class="${item.status}">
  <input ${item.status === 'completed' ? 'checked' : ''} type="checkbox" onclick="changeItemStatus(${item.id})"/>
  <span id=${item.id} onclick="makeEditable(this)" onkeydown="editItem(this)" onmouseout="makeDiseditable(this)">
  ${item.text}
  </span>
  <button class="deleteBtn" onclick="deleteItem(${item.id})">
  </button>
  </li>`
};

function filterItems(dom, status) {
  Array.from(dom.parentNode.children).forEach(element => element.classList.remove('btn-checked'));
  dom.classList.add('btn-checked');
  window.status = status;
  showItems(status);
}

function showItems(status) {
  window.status = status;
  let itemsHTML = (status === 'all') ? getAllItems().reduce((a,b) => a + itemGenerator(b), '') : getAllItems().filter(item => item.status === status).reduce((a,b) => a + itemGenerator(b), '');
  document.getElementById('items').innerHTML = itemsHTML;
};

function addToDoItem() {
  let items = getAllItems();
  let itemDOM = document.getElementById('addItem');
  if(itemDOM.value !== '') {
    let item = {
      id: items.length + 1,
      text: itemDOM.value,
      status: 'active'
    };
    items.push(item);
    saveAllItems(items);
    showItems(window.status);
    itemDOM.value = '';
  }
}

function deleteItem(id){
  let i = 1;
  let items = getAllItems().filter((item) => item.id !== id);
  items.map(item => item.id=i++);
  saveAllItems(items);
  showItems(window.status);
}

function changeItemStatus(id) {
  let items = getAllItems();
  items.forEach(item => item.status = item.id === parseInt(id) ? (item.status === 'completed' ? 'active' : 'completed') : item.status);
  saveAllItems(items);
  showItems(window.status);
}

function makeDiseditable(dom) {
  dom.setAttribute('contenteditable',true);
}

function makeEditable(dom) {
  dom.setAttribute('contenteditable',false);
}

function editItem(dom) {
  if(event.keyCode === 13) {
    let text = dom.innerText;
    let id = dom.getAttribute('id');
    let items = getAllItems();
    items.forEach(item => item.text = item.id === parseInt(id) ? text: item.text);
    saveAllItems(items);
    showItems(window.status);
  }
}